import sys, os

def Program():
    directory = os.listdir( "." )
    folderList = []

    width = 1224/2
    height = 1584/2

    version = "1-1"

    print( "COMIC PREPARER" )

    print( "" )
    print( "Directories" )

    for d in directory:
        if ( ".xcf" not in d and ".jpg" not in d and ".txt" not in d and ".py" not in d ):
            print( str( len( folderList ) ) + ". \t " + d )
            folderList.append( d )

    print( "" )

    idx = input( "Process the files in which directory? " )

    language = raw_input( "What language is it? " )

    version = raw_input( "What version is it? " )

    outputFile = "Rachels-Reader-1_" + language + "_" + version + ".pdf"

    command1 = "convert  -resize " + str( width ) + "x" + str( height ) + " \"" + folderList[idx] + "\"/*.jpg \"" + folderList[idx] + "\"/resized.jpg"
    print( "\n Resizing..." )
    print( "\t" + command1 )
    os.system( command1 )

    workDirectory = "\"" + folderList[idx] + "\"/"

    subdirectory = os.listdir( "./" + folderList[idx] )
    fileListString = ""

    fileCount = 0
    for d in subdirectory:
        if ( "resized" in d ):
            fileCount = fileCount + 1

    for i in range( fileCount ):
        fileListString = fileListString + "  " + workDirectory + "resized-" + str( i ) + ".jpg"

    command2 = "convert " + fileListString + "  " + workDirectory + outputFile
    print( "\n Converting to PDF..." )
    print( "\t" + command2 )
    os.system( command2 )

    command3 = "rm " + workDirectory + "resized*.jpg"
    print( "\n Cleanup..." )
    print( "\t" + command3 )
    os.system( command3 )


done = False

while( done == False ):
    Program()

    print( "" )
    choice = raw_input( "Exit? (y/n): " )

    if ( choice == "y" or choice == "Y" ):
        done = True
