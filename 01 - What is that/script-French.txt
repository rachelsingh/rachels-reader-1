
--------------------------------------------
Page 1
TITLE:      Qu'est-ce que c'est?

--------------------------------------------
Page 2
CGA the alien
(Don't replace "CGA", that's the name. If it's a different script, keep it as CGA?)

--------------------------------------------
Page 3
Little girl sees a UFO.

Little girl: Qu'est-ce que c'est que ça?

An alien beams down.

Alien: BONJOUR!

--------------------------------------------
Page 4

The girl throws the ball to the alien. The alien catches it.

Alien: Qu'est-ce que c'est?

The alien throws it back to another child.

Little boy: C'est un ballon!

--------------------------------------------
Page 5

Suddenly, a dog jumps out and steals the ball.

Alien: QU'EST-CE QUE C'EST QUE ÇA?

Girl: Ça, c'est un chien!

--------------------------------------------
Page 6

The alien chases the dog

Alien: Salut chien!

Alien: Chien!

As the alien chases the dog, a rock hits her in the back of the head.

Alien: Aïe!

Alien: Quelque chose m'a frappé!

--------------------------------------------
Page 7

The alien looks at a boy there.

Alien: M'as-tu frappé?

Little boy: non, je ne t'ai pas frappé.

The boy points to the ground

Little boy: Cette roche t'as frappé.

--------------------------------------------
Page 8

The alien picks up the rock

Alien: C'est une roche?

Boy: Oui, c'est une roche.

The alien glares at the rock.

Alien: Tu es méchante.

The rock says nothing.

Rock: ...

--------------------------------------------
Page 9

The alien finds a stall with a guy selling food.

Man: Bienvenue!

Man: Tout est en vente!

Alien: JE PEUX TOUT AVOIR?

Man: Non, tu peux ACHETER quelque chose.

--------------------------------------------
Page 10

Alien: J'ai une roche.

Alien: Qu'est-ce que je peux acheter?

Man: Je ne veux pas d'une roche.

Man: Je veux de l'argent.

Alien: Je n'ai pas d'argent.

Man: Alors tu ne peux rien acheter.

--------------------------------------------
Page 11

Alien: Je ne peux rien avoir?

Man: Tu ne peux rien avoir.

The alien leaves.

--------------------------------------------
Page 12

The alien throws her rock in the lake.

Alien: Maintenant je n'ai plus rien.

Alien: Je n'ai rien.

The alien is hit by a ball.

Alien: Ouille!

Alien: J'ai un ballon!

--------------------------------------------
Page 13

The kids from earlier run over.

Girl 1: Notre ami est là!

Girl 2: Lance le ballon!

Alien: J'ai des amis!

Alien: J'arrive!

Then they play ball

TEXT:   FIN










